﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BackpropagationNetwork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BackpropagationNetwork.Base;

namespace BackpropagationNetwork.Tests
{
    [TestClass()]
    public class BackpropagationNetworkTests
    {
        [TestMethod()]
        public void CalculateNeuronTest()
        {
            var counter = 1;
            var sum = 0;
            var neuronCount = 5;
            var neuronsL1 = new List<Neuron>(neuronCount);
            var neuronL2 = new Neuron() { Activation = x => x };
            var backpropagationNetwork = new BackpropagationNetwork();

            for (int i = 0; i < neuronCount; i++)
            {
                sum += counter;
                neuronsL1.Add(new Neuron() { Input = counter++, Activation = x => x });
                var weight = new Weight() { Value = 1, Start = neuronsL1[i], End = neuronL2 };
                neuronL2.InputWeight.Add(weight);
            }
            var real = backpropagationNetwork.CalculateNeuron(neuronL2);
            var expected = sum;
            Assert.IsTrue(real == expected);
        }

        [TestMethod()]
        public void CalculateDeltaHiddenNeuronSigmoidTest()
        {
            var counter = 1;
            var sum = 0;
            var neuronCount = 5;
            var neuronsL2 = new List<Neuron>(neuronCount);
            var neuronL1 = new Neuron() { Activation = x => x };
            var backpropagationNetwork = new BackpropagationNetwork();

            for (int i = 0; i < neuronCount; i++)
            {
                sum += counter;
                neuronsL2.Add(new Neuron() { Delta = counter++, Activation = x => x });
                var weight = new Weight() { Value = 1, Start = neuronL1, End = neuronsL2[i] };
                neuronL1.OutputWeight.Add(weight);
            }

            backpropagationNetwork.CalculateDeltaHiddenNeuronSigmoid(neuronL1);
            var real = neuronL1.Delta;
            var expected = sum * backpropagationNetwork.DerivativeSigmoid(neuronL1);
            Assert.IsTrue(real==expected);
        }
    }
}