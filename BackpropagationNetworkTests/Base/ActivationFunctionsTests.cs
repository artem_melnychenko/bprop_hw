﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BackpropagationNetwork.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackpropagationNetwork.Base.Tests
{
    [TestClass()]
    public class ActivationFunctionsTests
    {
        [TestMethod()]
        public void SigmoidTest()
        {
            var val = 12;
            var activationFunctions = new ActivationFunctions();
            var real = activationFunctions.Sigmoid(val);
            var expected = 1 / (1 + Math.Exp(-val));
            Assert.IsTrue(real == expected);
        }


    }
}