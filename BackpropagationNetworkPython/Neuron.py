

class Neuron:
    def __init__(self, activation):    
        self.input = 0
        self.activation = activation
        self.delta =0
        self.bias = 1
        self.input_weight =[] 
        self.output_weight=[]
    def output(self):
        return self.activation(self.input)

if __name__=='__main__':
    neuron = Neuron(lambda x:x*x)
    neuron.input = 5
    print('input = ', neuron.input, ' act = x*x  output =' ,neuron.output())
