from Neuron import Neuron
from Weight import Weight
from ActivationFunctions import ActivationFunctions
from CalculatingError import CalculatingError
from Layer import Layer

class BackpropagationNetwork:
    def __init__(self):
        self.calc_error = CalculatingError()
        self.layers = []
        self.learning_rate = 0.01
        self.momentum = 0.3

    def input_layer(self):
        return self.layers[0]
    def output_layer(self):
        return self.layers[-1]
    def hidden_layers(self):
        return self.layers[1:-1]
    def layers_count(self):
        return len(self.layers)
    def connect_layers(self):
        for i in range(self.layers_count() - 1):
            self.layers[i].connect_to_next_layer(self.layers[i + 1])
    def calculate_neuron(self, neuron):
        neuron.input = sum((weight.value * weight.start.output()) for weight in neuron.input_weight)

        if (neuron.bias != None):
            neuron.input += neuron.bias

        return neuron.output()

    def calculate_output(self, inputs=None):
        if inputs != None:
            for i in range(len(self.input_layer().neurons)):
                self.input_layer().neurons[i].input = inputs[i]
        for i in range(1,len(self.layers)):
            for neuron in self.layers[i].neurons:
                self.calculate_neuron(neuron)

    def calculate_error(self, expected):
        real = [ output.output() for output in self.output_layer().neurons]
        error = self.calc_error.rootMSE(real, expected)
        return error

    def train(self, inputs, expected, epochs, writer):
        for epoch in range(epochs):
            epoch_error = []
            sets = len(inputs)
            for set in range(sets):
                input = [float(x) for x in inputs[set].split(' ')]
                expected_result = [float(x) for x in expected[set].split(' ')]
                for i in range(len(self.input_layer().neurons)):
                    self.input_layer().neurons[i].input = input[i]
                self.calculate_output()
                result = [x.output() for x in self.output_layer().neurons]
                error = self.calc_error.rootMSE(result, expected_result)
                epoch_error.append(error)
                self.weights_calibration(expected_result)
            mid_error = sum(epoch_error) / len(epoch_error)
            if (epoch + 1) % 5 == 0:
                if writer != None:
                    writer("Epoch",epoch + 1," ; error = ",mid_error,"%")
            if mid_error < 0.05:
                writer("Epoch",epoch + 1," ; error = ",mid_error,"%")
                
    def weights_calibration(self, expected):
        for i in range(len(self.output_layer().neurons)):
            self.calculate_delta_output_neuron_sigmoid(self.output_layer().neurons[i], expected[i])
        for neuron in self.output_layer().neurons:
            for weight in neuron.input_weight:
                weight.calc_delta_weight(self.learning_rate, self.momentum)
                weight.update_weight()

        self.hidden_layers().reverse()
        reverse_hidden_layers = self.hidden_layers()
        for layer in reverse_hidden_layers:
            for i in range(len(layer.neurons)):
                self.calculate_delta_hidden_neuron_sigmoid(layer.neurons[i])
            for neuron in layer.neurons:
                for weight in neuron.input_weight:
                    weight.calc_delta_weight(self.learning_rate, self.momentum)
                    weight.update_weight()
        self.hidden_layers().reverse()

    def calculate_delta_output_neuron_sigmoid(self, neuron, expected):
        neuron.delta = (expected - neuron.output())* self.derivative_sigmoid(neuron)

    def calculate_delta_hidden_neuron_sigmoid(self, neuron):
        neuron.delta = self.derivative_sigmoid(neuron) * sum([(weight.value * weight.end.delta) for weight in neuron.output_weight])

    def derivative_sigmoid(self, neuron):
        return (1 - neuron.output())*neuron.output()


if __name__ == '__main__':
   bprop = BackpropagationNetwork()
   bprop.learning_rate = 0.3
   bprop.momentum = 0.9
   activ_func = ActivationFunctions()
   input_layer = Layer(3, False, activ_func.line)
   hidden_layer1 = Layer(6, True, activ_func.sigmoid)
   output_layer = Layer(1, False, activ_func.sigmoid)
   bprop.layers.append(input_layer)
   bprop.layers.append(hidden_layer1)
   bprop.layers.append(output_layer)
   bprop.connect_layers()
   inputs = ['0.11 0.2 0.3','0.91 0.87 0.94','0.11 0.23 0.24','0.12 0.33 0.24','0.87 0.96 0.87','0.19 0.29 0.31','0.98 0.81 0.99','0.09 0.29 0.26','0.17 0.38 0.26','0.98 0.98 0.88']
   expected = ['1','0','1','1','0','1','0','1','1','0']
   graph = bprop.train(inputs, expected, 1000, print)

