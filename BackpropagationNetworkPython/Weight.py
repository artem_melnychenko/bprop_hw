from Neuron import Neuron

class Weight:
    def __init__(self, start_neuron, end_neuron):
        self.value = 0
        self.delta_weight = 0
        self.delta_weight_prev = 0
        self.start = start_neuron
        self.end = end_neuron

    def GRAD(self):
        return self.start.output() * self.end.delta

    def calc_delta_weight(self, learning_rate, momentum):
        self.delta_weight = learning_rate * self.GRAD() + self.delta_weight_prev * momentum
        self.delta_weight_prev = self.delta_weight

    def update_weight(self):
        self.value += self.delta_weight


if __name__=='__main__':
    start_neuron = Neuron(lambda x: x*x)
    start_neuron.input = 5
    end_neuron = Neuron(lambda x: x)
    end_neuron.delta = 5

    weight = Weight(start_neuron, end_neuron)
    weight.value = 2
    print('grad = ', weight.GRAD())
    weight.calc_delta_weight(0.2,0.1)
    print('delta_weight = ', weight.delta_weight)
    print('prev weight = ', weight.value)
    weight.update_weight()
    print('new weight = ', weight.value)



