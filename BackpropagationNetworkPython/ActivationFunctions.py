import math

class ActivationFunctions:
    def sng(self, value):
        if value > 0.5:
            return 1
        return 0

    def line(self, value):
        return value
    
    def sigmoid(self, value):
        return 1 / (1 + math.exp(-value))
    
    def hyperbolicTangent(self, value):
        return (math.exp(2*value) - 1)/(math.exp(2*value) + 1)

if __name__=='__main__':
    act_func = ActivationFunctions()
    print('sng 0.2 = ', act_func.sng(0.2))    
    print('sng 0.7 = ', act_func.sng(0.7))
    print('line 0.123 = ', act_func.line(0.123))
    print('sigmoid 0.3 = ', act_func.sigmoid(0.3))
    print('tanh 0.3 = ', act_func.hyperbolicTangent(0.3))