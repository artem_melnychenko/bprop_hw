﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackpropagationNetwork.Base
{
    public class Weight
    {
        public double Value { get; set; }
        public double DeltaWeight { get; private set; }
        public double DeltaWeightPrev { get; private set; }
        public Neuron Start { get; set; }
        public Neuron End { get; set; }
        public Weight() => DeltaWeightPrev = 0;
        public double GRAD() => Start.Output * End.Delta;
        public void CalcDeltaWeight(double learningRate, double momentum)
        {
            DeltaWeight = learningRate * GRAD() + DeltaWeightPrev * momentum;
            DeltaWeightPrev = DeltaWeight;
        }
        public void UpdateWeight() => Value += DeltaWeight;

    }
}
